const { Router } = require('express');


const router = Router();
const express = require('express');
// const session = require('express-session')
const { request, response } = require('./markets');



const PORT = 3001;



const groceries = [
    {
        item: 'bread',
        quantity: 3,
    },
    {
        item: 'coffee',
        quantity: 1,
    },
    {
        item: 'sugar',
        quantity: 1
    }
]
router.get('',

    (request, response, next) => {
        response.cookie('visited', true, {
            maxAge: 10000,

        }
        );
        response.send(groceries)

    });


router.get('/:item', (request, response) => {
    const { item } = request.params;
    const groceryItem = groceries.find((g) => g.item === item);
    response.send(groceryItem);
})

router.post('', (request, response, next) => {
    groceries.push(request.body);
    response.send(201);
});

router.get('/shopping/cart' ,(request, response)=>{
    const {cart} = request.session;
    if(!cart){
        response.send('you have no cart session');
    }else{
        response.send(cart);
    }
});
router.post('/shopping/cart/item', (request,response)=>{
    const{item ,quantity} = request.body;
    const cartItem = {item , quantity};
    const {cart} = request.session;
    if(cart){
        
        request.session.cart.items.push({
           items :[cartItem]
         })
         
    }else{
        request.session.cart = {
            items :[cartItem],
        }
    }
    response.send(201);

})
module.exports = router;