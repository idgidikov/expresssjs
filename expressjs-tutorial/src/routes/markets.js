 const Router = require('express');
const { default: mongoose } = require('mongoose');


  const router = Router();
const supermarkets = [
    {   id : 1,
        store : 'Kaufland',
        miles: 4.4
    },
    {
        id:2,
        store : 'Lidl',
        miles: 5.5
    }
]



router.get('',(request ,response)=>{
    const {miles} = request.query
    const parsedMiles = parseInt(miles);
    if( !isNaN(parsedMiles)){
        const filteredStores = supermarkets.filter((s)=> s.miles <= parsedMiles);
        response.send(filteredStores);
    }
    else{
        response.send(supermarkets);
}
   
})

module.exports=router;

