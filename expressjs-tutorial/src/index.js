
const express = require('express');
const cookieParser = require('cookie-parser');
const marketsRoute =require('./routes/markets');
const groceriesRoute = require('./routes/groceries');
const session = require('express-session');
const authRoute = require('./routes/auth');
const app = express();
require('./database');


const PORT = 3001;



app.use(express.json()) // allow send json to the server
app.use(express.urlencoded());
app.use(cookieParser());
app.use(
    session({
        secret: 'AAAAFAFAFA',
        resave:false,
        saveUninitialized : false,
    })
)
app.use((req,res,next)=>{
    console.log(`${req.url} : ${req.method}`);
    next();
});

app.use('/api/groceries',groceriesRoute);
app.use('/api/markets',marketsRoute);
app.use('/api/v1/auth', authRoute);
app.listen(PORT, () => console.log(`Running express server on port ${PORT}`));
// const groceries = [
//     {
//         item: 'bread',
//         quantity: 3,
//     },
//     {
//         item: 'coffee',
//         quantity: 1,
//     },
//     {
//         item: 'sugar',
//         quantity: 1
//     }
// ]
//app.get('/groceries', 
// (request , response , next) =>{
//     console.log('Before Handling Request');
//     next();
// },
// (request, response, next) => {
//     response.send(groceries)
//     next();
// },
// (request, response, next) => {
//     console.log('Finished executing get request')
// });

// app.get('/groceries/:item',(request, response)=>{
//     const {item} = request.params;
//     const groceryItem = groceries.find((g) => g.item ===item);
//     response.send(groceryItem);
// })

// app.post('/groceries', (request , response ,next)=> {
//     groceries.push(request.body);
//     response.send(201);
// });